import 'package:app_food/Providers/cart.dart';
import 'package:app_food/Providers/food_type.dart';
import 'package:app_food/screens/detail_item.dart';
import 'package:app_food/screens/italian_food.dart';
import 'package:app_food/screens/my_order.dart';
import 'package:flutter/material.dart';
import 'package:app_food/screens/home_page.dart';
import 'package:app_food/Providers/food_items.dart';
import 'package:provider/provider.dart';
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return 
    MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (ctx) => Foods(),),
        ChangeNotifierProvider(
          create: (ctx) => Cart(),),
        ChangeNotifierProvider(
          create: (ctx) => Foodtypes(),),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
        home: HomePage(),
      ),
    );
  }
}