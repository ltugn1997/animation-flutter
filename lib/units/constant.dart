import 'package:app_food/models/food_item.dart';
import 'package:flutter/material.dart';

class Contants{
  static OutlineInputBorder border = OutlineInputBorder(
    borderRadius: BorderRadius.circular(30),
    borderSide: BorderSide(color: Colors.transparent),
  );

  static String avataUrl = 'https://images.unsplash.com/photo-1522778147829-047360bdc7f6?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=658&q=80';

  static List<Food> dishItems = [
    Food(id:'1', image:'assets/plate1.png', name: 'Chile Rice', cost: 10, calorie: 500, squirrels: 60, fats: 33, entrance: 400, description: 'The flavor of your food is what most customers focus on when they are deciding what to eat.',
     rawMaterial: ['assets/beef.png', 'assets/carrot.png', 'assets/mushrooms.png', 'assets/spinach.png', 'assets/tomato.png']),
    
    Food(id:'2',  image:'assets/plate2.png', name: 'Beef Steak', cost: 20, calorie: 450, squirrels: 60, fats: 35, entrance: 450, description: 'The flavor of your food is what most customers focus on when they are deciding what to eat.',
     rawMaterial: ['assets/beef.png', 'assets/carrot.png', 'assets/mushrooms.png', 'assets/spinach.png', 'assets/tomato.png']),
    
    Food(id:'3',  image:'assets/plate3.png', name: 'Fish Fry', cost: 15, calorie: 350, squirrels: 60, fats: 23, entrance: 250, description: 'The flavor of your food is what most customers focus on when they are deciding what to eat.',
     rawMaterial: ['assets/beef.png', 'assets/carrot.png', 'assets/mushrooms.png', 'assets/spinach.png', 'assets/tomato.png']),
    
    Food(id:'4',  image:'assets/plate4.png',name: 'Pork Chops', cost: 23, calorie: 450, squirrels: 60, fats: 43, entrance: 350, description: 'The flavor of your food is what most customers focus on when they are deciding what to eat.',
     rawMaterial: ['assets/beef.png', 'assets/carrot.png', 'assets/mushrooms.png', 'assets/spinach.png', 'assets/tomato.png'])
  ];
  
  static List carts =[
    {
    'image':'assets/tuna.png',
    'title':'Japanese',
    'color':Colors.red.withOpacity(0.5),
    'colorText':Colors.red,
    'profileImage':'assets/plate1.png',
    'menu':typeItems,
    'typeFood':['Pasta', 'Pizza', 'Basilico', 'Panzenella', 'Bruschetta'],
    },
    {
    'image':'assets/pizza.png',
    'title':'Italian',
    'color': Colors.orange.withOpacity(0.5),
    'colorText':Colors.orange,
    'profileImage':'assets/plate2.png',
    'menu':typeItems,
    'typeFood':['Pasta', 'Pizza', 'Basilico', 'Panzenella', 'Bruschetta'],
    },
    {
    'image':'assets/shrimp.png',
    'title':'Chinese',
    'color': Colors.blue.withOpacity(0.5),
    'colorText':Colors.blue,
    'profileImage':'assets/plate3.png',
    'menu':typeItems,
    'typeFood':['Pasta', 'Pizza', 'Basilico', 'Panzenella', 'Bruschetta'],
    },
    {
    'image':'assets/vegetables.png',
    'title':'America',
    'color': Colors.green.withOpacity(0.5),
    'colorText':Colors.green,
    'profileImage':'assets/plate4.png',
    'menu':typeItems,
    'typeFood':['Pasta', 'Pizza', 'Basilico', 'Panzenella', 'Bruschetta'],
    },
  ];

  static List recommendItems =[
    {
      'image':'assets/plate1.png',
      'name':'Chile Rice',
      'cost':10,
      'calorie':  500,
      'Squirrels': 60,
      'Fats': 33,
      'entrance': 450,
      'description':'The flavor of your food is what most customers focus on when they are deciding what to eat.',
      'rawMaterial': ['assets/beef.png', 'assets/carrot.png', 'assets/mushrooms.png', 'assets/spinach.png', 'assets/tomato.png']
    },
    {
      'image':'assets/plate2.png',
      'name':'Beef Steak',
      'cost':13,
      'calorie':  500,
      'Squirrels': 60,
      'Fats': 33,
      'entrance': 350,
      'description':'The flavor of your food is what most customers focus on when they are deciding what to eat.',
      'rawMaterial': ['assets/beef.png', 'assets/carrot.png', 'assets/mushrooms.png', 'assets/spinach.png', 'assets/tomato.png']
    },
    {
      'image':'assets/plate3.png',
      'name':'Fish Fry',
      'cost':8,
      'calorie':  500,
      'Squirrels': 60,
      'Fats': 33,
      'entrance': 250,
      'description':'The flavor of your food is what most customers focus on when they are deciding what to eat.',
      'rawMaterial': ['assets/beef.png', 'assets/carrot.png', 'assets/mushrooms.png', 'assets/spinach.png', 'assets/tomato.png']
    },
    {
      'image':'assets/plate4.png',
      'name':'Pork Chops',
      'cost':10,
      'calorie':  500,
      'Squirrels': 60,
      'Fats': 33,
      'entrance': 450,
      'description':'The flavor of your food is what most customers focus on when they are deciding what to eat.',
      'rawMaterial': ['assets/beef.png', 'assets/carrot.png', 'assets/mushrooms.png', 'assets/spinach.png', 'assets/tomato.png']
    },
  ];

  static List typeItems =[
    {
      'image':'assets/plate1.png',
      'name':'Chile Rice',
      'cost':10,
      'calorie':  500,
      'Squirrels': 60,
      'Fats': 33,
      'entrance': 450,
      'description':'The flavor of your food is what most customers focus on when they are deciding what to eat.',
      'rawMaterial': ['assets/beef.png', 'assets/carrot.png', 'assets/mushrooms.png', 'assets/spinach.png', 'assets/tomato.png']
    },
    {
      'image':'assets/plate2.png',
      'name':'Beef Steak',
      'cost':13,
      'calorie':  500,
      'Squirrels': 60,
      'Fats': 33,
      'entrance': 350,
      'description':'The flavor of your food is what most customers focus on when they are deciding what to eat.',
      'rawMaterial': ['assets/beef.png', 'assets/carrot.png', 'assets/mushrooms.png', 'assets/spinach.png', 'assets/tomato.png']
    },
    {
      'image':'assets/plate3.png',
      'name':'Fish Fry',
      'cost':8,
      'calorie':  500,
      'Squirrels': 60,
      'Fats': 33,
      'entrance': 250,
      'description':'The flavor of your food is what most customers focus on when they are deciding what to eat.',
      'rawMaterial': ['assets/beef.png', 'assets/carrot.png', 'assets/mushrooms.png', 'assets/spinach.png', 'assets/tomato.png']
    },
    {
      'image':'assets/plate4.png',
      'name':'Pork Chops',
      'cost':10,
      'calorie':  500,
      'Squirrels': 60,
      'Fats': 33,
      'entrance': 450,
      'description':'The flavor of your food is what most customers focus on when they are deciding what to eat.',
      'rawMaterial': ['assets/beef.png', 'assets/carrot.png', 'assets/mushrooms.png', 'assets/spinach.png', 'assets/tomato.png']
    },
  ];

  static List itemMyOrder =[
    {
      'image':'assets/plate1.png',
      'name':'Chile Rice',
      'cost':10,
      'calorie':  500,
      'Squirrels': 60,
      'Fats': 33,
      'entrance': 450,
      'description':'The flavor of your food is what most customers focus on when they are deciding what to eat.',
      'rawMaterial': ['assets/beef.png', 'assets/carrot.png', 'assets/mushrooms.png', 'assets/spinach.png', 'assets/tomato.png']
    },
    {
      'image':'assets/plate2.png',
      'name':'Beef Steak',
      'cost':13,
      'calorie':  500,
      'Squirrels': 60,
      'Fats': 33,
      'entrance': 350,
      'description':'The flavor of your food is what most customers focus on when they are deciding what to eat.',
      'rawMaterial': ['assets/beef.png', 'assets/carrot.png', 'assets/mushrooms.png', 'assets/spinach.png', 'assets/tomato.png']
    },
  ];
}

