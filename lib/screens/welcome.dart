import 'package:app_food/Providers/food_items.dart';
import 'package:app_food/models/food_item.dart';
import 'package:app_food/units/constant.dart';
import 'package:app_food/widgets/categories_card.dart';
import 'package:app_food/widgets/recommend_card.dart';
import 'package:app_food/widgets/search.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class WelcomeScreen extends StatefulWidget {
  final Function(dynamic, String) onPush;
  WelcomeScreen({Key key, this.onPush}) : super(key: key);

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  @override
  Widget build(BuildContext context) {
    final foodsData = Provider.of<Foods>(context);
    final foods = foodsData.items; 
    return SafeArea( 
        child:  Scaffold(
          appBar: AppBar(
            leading: null,
            backgroundColor: Colors.transparent,
            centerTitle: false,
            elevation: 0.0,
            title: titleWelcome(),
          ),
          body: Padding(
            padding: EdgeInsets.symmetric(horizontal: 30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SearchWidget(),
                Categories(onPush: widget.onPush),
                SizedBox(height: 15,),
                Text('Recommend', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),),
                Expanded(
                  child: ListView.builder(
                    itemBuilder: (ctx, i) {
                      Food recommendItem = foods[i];
                      String image = recommendItem.image;
                      return InkWell(
                        onTap: (){ widget.onPush(recommendItem, '/detail_item');},
                        child: Hero(
                          tag: (image),
                          child: RecommendCard(
                          imgURl: image,
                          cost: recommendItem.cost.toStringAsFixed(2),
                          name: recommendItem.name,
                          entrance: recommendItem.entrance,
                          ),
                        ),
                      );
                    },
                    itemCount: foods.length,
                  )
                )
              ],    
            ),
          ),
        ),  
    );
  }
}

Widget titleWelcome(){
  return Container(
    padding: EdgeInsets.symmetric(horizontal: 10),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          "Hi Flutter",
          style: TextStyle(
            fontSize: 25.0,
            letterSpacing: 1.5,
            fontWeight: FontWeight.bold,
            color: Colors.black
          ),
        ),
        Material(
          elevation: 40.0,
          shadowColor: Colors.grey,
          borderRadius: BorderRadius.circular(50),
          child: CircleAvatar(   
            backgroundImage: NetworkImage(Contants.avataUrl),
            radius: 18,
         )
       )
      ],
    ),
  );
}