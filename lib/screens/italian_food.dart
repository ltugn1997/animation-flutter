import 'package:app_food/Providers/food_type.dart';
import 'package:app_food/models/food_item.dart';
import 'package:app_food/widgets/recommend_card.dart';
import 'package:flutter/material.dart';

class ItalianFood extends StatefulWidget {
  final Foodtype argument;
  final Function(dynamic, String) onPush;
  ItalianFood({Key key, this.argument, this.onPush}) : super(key: key);

  @override
  _ItalianFoodState createState() => _ItalianFoodState();
}

class _ItalianFoodState extends State<ItalianFood> with SingleTickerProviderStateMixin{
    GlobalKey<AnimatedListState> listKey = GlobalKey<AnimatedListState>();
    List<Widget> foodItems = [];
    Animation animation;
    AnimationController animationController;
    int isSelected = 0;

    @override
    void initState() {
      super.initState();
      animationController = AnimationController(vsync: this, duration: Duration(milliseconds: 200));

      animation = Tween(begin: 100.0, end: 300.0).animate(animationController);

      WidgetsBinding.instance.addPostFrameCallback((_) {
        final Foodtype arguments = widget.argument;
        addFoodItems(arguments);
      });
    }

    void addFoodItems(Foodtype arguments) {
    List<Food> foods = arguments.foods;
    Future future = Future((){});
    foods.forEach((food) {
      future = future.then((data) {
        return Future.delayed(const Duration(milliseconds: 100), () {
           foodItems.add(buildTile(food));
          listKey.currentState.insertItem(foodItems.length - 1);
        });
      });
    });
  }

  Widget buildTile(Food food) {
    return InkWell(
        onTap: (){ widget.onPush(food, '/detail_item');},
        child: 
          RecommendCard(
          imgURl: food.image,
          cost: food.cost.toStringAsFixed(2),
          name: food.name,
          entrance: food.entrance,
          ),
      );
  }

  Tween<Offset> _offset = Tween(begin: Offset(0, 2), end: Offset(0, 0));

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final Foodtype arguments = widget.argument;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.orange[200],
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          leading: IconButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            icon: Icon(Icons.arrow_back_ios),
            color: Colors.black,
          ),
          backgroundColor: Colors.transparent,
          elevation: 0.0,
        ),
        body: ListView(
          padding: EdgeInsets.only(bottom: 30),
          shrinkWrap: true,
          children: [
          Stack(
            overflow: Overflow.visible,
            children: [
              Container(
                height: size.height,
                width: size.width,
                color: Colors.transparent,
              ),
              Positioned(
                top: -75,
                right: -45,
                child: FittedBox(  
                  child: Container(
                    height: size.height*0.4,
                    child: 
                    Hero(
                      tag: arguments.profileImage + 'type',
                      child: Image.asset(arguments.profileImage),
                    ),
                  ),
                ),   
              ),
              Positioned(
                  top: size.height * 0.25,
                  child: 
                    TweenAnimationBuilder(
                      tween: Tween<double>(begin: 0, end: 1),
                      duration: Duration(milliseconds: 500),
                      curve: Curves.easeIn,
                      builder: (BuildContext context, double _val, Widget child) {
                      return Container(
                        margin: EdgeInsets.only(left: _val * 30),
                        child: Text(
                          arguments.title, style: TextStyle(
                          color: Colors.red, 
                          fontWeight: FontWeight.bold,
                          fontSize: 25),),  
                      );
                    }
                  )         
                ),          
                Positioned(
                  top: size.height * 0.35,
                  child: Container(
                    height: size.height,
                    width: size.width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(45.0),
                      ),
                      color: Colors.white
                    ),
                  ),
                ),
                Positioned(
                  top: size.height * 0.32,
                  right: 0,
                  child:AnimatedBuilder(
                    animation: animationController,
                    builder:(BuildContext context, Widget child){
                      return 
                      Material(
                        elevation: 18.0,
                        shadowColor: Colors.grey,
                        color: Colors.red,
                        borderRadius: BorderRadius.only(topLeft: Radius.circular(15), bottomLeft: Radius.circular(15)),
                        child: Container(
                          height: 50,
                          width: animation.value,
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            shrinkWrap: true,
                            itemBuilder: (ctx, i){
                              return InkWell(
                                onTap: () {
                                  animationController.forward();
                                  setState(() {
                                    isSelected = i;
                                  });
                                },
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                  Container(
                                    padding: EdgeInsets.symmetric(horizontal: 5),
                                    child: Center(
                                    child: Text(arguments.type[i], style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),)
                                  ),),
                                  (isSelected == i)? 
                                  Container(child: Text('•',style: TextStyle(color: Colors.white, fontSize: 20,fontWeight: FontWeight.bold),))
                                  : Container(child: Text('•',style: TextStyle(color: Colors.transparent, fontSize: 20,fontWeight: FontWeight.bold),))
                                ],),
                              );   
                            },
                            itemCount: arguments.type.length,
                          )
                        )
                      );
                    }
                  )     
                ),
                Positioned(
                top: size.height * 0.4,
                left: 30,
                right: 30,
                child: AnimatedList(
                    key: listKey,
                    initialItemCount: foodItems.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index, animation) {
                      return SlideTransition(
                        position: animation.drive(_offset),
                        child: foodItems[index],
                      );
                    }
                  ),
                ),
              ],
            ),
          ],
        )
      ),
    );
  }
}