import 'package:app_food/Providers/cart.dart';
import 'package:app_food/models/food_item.dart';
import 'package:app_food/widgets/my_order_cart.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MyOrder extends StatefulWidget {
  MyOrder({Key key}) : super(key: key);

  @override
  _MyOrderState createState() => _MyOrderState();
}

class _MyOrderState extends State<MyOrder> {
  @override
  Widget build(BuildContext context) {
    final cart = Provider.of<Cart>(context);
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text('My Orther', style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold, color: Colors.black),),
      ),
      body: 
      Column(children: [
        Expanded(
          child: ListView.builder(
            itemBuilder: (ctx, i) {
              CartItem dataRecommendItem = cart.items.values.toList()[i];
              Food recommendItem = dataRecommendItem.food;
              return Column(children: [
                  Hero(
                    tag: (recommendItem.image),
                    child: Dismissible(
                      key: ValueKey(dataRecommendItem.id),
                      background: Container(
                        color: Colors.red,
                        child: Icon(Icons.delete, color: Colors.white, size: 40,),
                        alignment: Alignment.centerRight,
                        padding: EdgeInsets.only(right: 20),
                        margin: EdgeInsets.symmetric(horizontal: 15, vertical: 4),
                      ),
                      direction: DismissDirection.endToStart,
                      onDismissed: (direction){
                        cart.removeItem(recommendItem.id);
                      },
                      child: MyOrderCard(
                      quantity: dataRecommendItem.quantity,
                      imgURl: recommendItem.image,
                      cost: recommendItem.cost.toStringAsFixed(2),
                      name: recommendItem.name,
                      entrance: recommendItem.entrance,),
                    ),
                ),
                Divider(
                  indent: 25,
                      color: Colors.black,
                    )
                 ],);
            },
            itemCount: cart.items.length,
          ),
        ),
        Stack(children: [
          Positioned(
            top: size.height*0.04,
            child: Container(
              height: size.height * 0.1, width: size.width, 
              decoration: BoxDecoration(color: Colors.blueGrey,),
            ),
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
            Container(height: size.height * 0.1, width: size.width/2, 
              decoration: BoxDecoration(color: Colors.blueGrey,),
              child: Center(
                child: Text('Total: ' + '\$ ${cart.totalAmount.toStringAsFixed(2)}', style: TextStyle(color: Colors.white, fontSize: 20),
              ),
            ),
          ),
          InkWell(
            onTap: (){},
            child: Container(height: size.height * 0.14, width: size.width/2,
              decoration: BoxDecoration(color: Colors.greenAccent,
              borderRadius: BorderRadius.only(topLeft: Radius.circular(65)),
              ),
              child: Center(
                child: Text('Confirm Order', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25, color: Colors.white),)
              ),
            )
          )
        ],),
        ],),
      ],)       
    );
  }
}