import 'package:app_food/Providers/cart.dart';
import 'package:app_food/models/food_item.dart';
import 'package:app_food/widgets/button_caculator.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
class DetailItem extends StatefulWidget {
  final Food argument;
  DetailItem({Key key, this.argument}) : super(key: key);

  @override
  _DetailItemState createState() => _DetailItemState();
}

class _DetailItemState extends State<DetailItem> {
  @override
  Widget build(BuildContext context) {
    final Cart cart = Provider.of<Cart>(context, listen: false);
    Size size = MediaQuery.of(context).size;
    final Food arguments = widget.argument;
    String price = arguments.cost.toStringAsFixed(2);
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.orange[200],
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          leading: IconButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            icon: Icon(Icons.arrow_back_ios),
            color: Colors.black,
          ),
          backgroundColor: Colors.transparent,
          elevation: 0.0,
        ),
        body: ListView(
          padding: EdgeInsets.only(bottom: 30),
          primary:true,
          shrinkWrap: true,
          children: [
          Stack(
            overflow: Overflow.visible,
            children: [
              Positioned(
                top: -75,
                right: -45,
                child: FittedBox(  
                  child: Container(
                    height: size.height*0.4,
                    child: 
                    Hero(
                      tag: arguments.image,
                      child: Image.asset(arguments.image),
                    ),
                  ),
                ),   
              ),
              Positioned(
                  top: size.height * 0.25,
                  left: 30,
                  child: Column(
                    crossAxisAlignment:CrossAxisAlignment.start,
                    children: [
                    Text(
                      arguments.name, style: TextStyle(
                      color: Colors.red, 
                      fontWeight: FontWeight.bold,
                      fontSize: 25),),
                    Text(
                      '\$$price', style: TextStyle(
                      color: Colors.red, 
                      fontWeight: FontWeight.bold,
                      fontSize: 25),)
                  ],),              
                ),
                Container(
                  height: size.height,
                  width: size.width,
                  color: Colors.transparent,
                ),
              Positioned(
                top: size.height * 0.35,
                child: Container(
                  height: size.height,
                  width: size.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(45.0),
                    ),
                    color: Colors.white
                  ),
                ),
              ),
              Positioned(
                top: size.height * 0.33,
                right: 30,
                child:Material(
                  elevation: 18.0,
                  shadowColor: Colors.grey,
                  borderRadius: BorderRadius.circular(30),
                  child: ButtonCaculator(quantity: 1),
                ),
              ),
              Positioned(
              top: size.height * 0.4,
              left: 30,
              right: 30,
              child:Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    child: Text(arguments.description, style: TextStyle(fontSize: 18),),
                  ),
                  SizedBox(height: 20,),               
                  infoNutrition(arguments),        
                  listRawMaterial(size, context, arguments),
                  buttonAddCart(size, cart, arguments, 1),
                ],
              )
            ),
            ],
          )
          ],
        )
      ),
    );
  }
}

Widget buttonAddCart(Size size, Cart cart, Food arguments, int quantity){
  return InkWell(
    onTap: (){ cart.addItem(arguments.id, arguments.cost, arguments, quantity: quantity);},
    child: Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      width: size.width,
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
      borderRadius: BorderRadius.all(
        Radius.circular(45.0),
      ),
      color: Colors.greenAccent                  
      ),
      child: Text('Add To Cart', textAlign: TextAlign.center,),
    ),
  );
}

Widget listRawMaterial(Size size, BuildContext context, Food arguments){
   return Container(
    margin: EdgeInsets.only(top: 20),
    height: size.height * 0.12,
    width: MediaQuery.of(context).size.width,
    child: ListView.builder(
      scrollDirection: Axis.horizontal,
      itemBuilder: (ctx, i){
        return Image.asset(arguments.rawMaterial[i]);
    },
    itemCount: arguments.rawMaterial.length,
    )
  );
}

Widget infoNutrition(Food arguments){
  return  Row(
    mainAxisAlignment: MainAxisAlignment.spaceAround,
    children: [
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(children: [
            Text('Calorie: ',style: TextStyle(fontWeight: FontWeight.bold),),
            Text(arguments.calorie.toString()+'cal'),
            ],),
          
          Row(children: [
            Text('Fats: ',style: TextStyle(fontWeight: FontWeight.bold)),
            Text(arguments.fats.toString()+'g'),
          ],)
        ],
      ),
      Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
          Row(children: [
          Text('Squirrels: ',style: TextStyle(fontWeight: FontWeight.bold),),
          Text(arguments.squirrels.toString()+'g'),
          ],),
        
          Row(children: [
          Text('Endtrance: ',style: TextStyle(fontWeight: FontWeight.bold)),
          Text(arguments.entrance.toString()+'g'),
        ],)
      ],
    )
  ],);  
}