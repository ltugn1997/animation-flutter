import 'package:flutter/material.dart';

import 'package:app_food/models/food_item.dart';

class Foods with ChangeNotifier{
  List<Food> _items = [
    Food(id:'1', image:'assets/plate1.png', name: 'Chile Rice', cost: 10, calorie: 500, squirrels: 60, fats: 33, entrance: 400, description: 'The flavor of your food is what most customers focus on when they are deciding what to eat.',
     rawMaterial: ['assets/beef.png', 'assets/carrot.png', 'assets/mushrooms.png', 'assets/spinach.png', 'assets/tomato.png']),
    
    Food(id:'2',  image:'assets/plate2.png', name: 'Beef Steak', cost: 20, calorie: 450, squirrels: 60, fats: 35, entrance: 450, description: 'The flavor of your food is what most customers focus on when they are deciding what to eat.',
     rawMaterial: ['assets/beef.png', 'assets/carrot.png', 'assets/mushrooms.png', 'assets/spinach.png', 'assets/tomato.png']),
    
    Food(id:'3',  image:'assets/plate3.png', name: 'Fish Fry', cost: 15, calorie: 350, squirrels: 60, fats: 23, entrance: 250, description: 'The flavor of your food is what most customers focus on when they are deciding what to eat.',
     rawMaterial: ['assets/beef.png', 'assets/carrot.png', 'assets/mushrooms.png', 'assets/spinach.png', 'assets/tomato.png']),
    
    Food(id:'4',  image:'assets/plate4.png',name: 'Pork Chops', cost: 23, calorie: 450, squirrels: 60, fats: 43, entrance: 350, description: 'The flavor of your food is what most customers focus on when they are deciding what to eat.',
     rawMaterial: ['assets/beef.png', 'assets/carrot.png', 'assets/mushrooms.png', 'assets/spinach.png', 'assets/tomato.png'])
  ];

  List<Food> get items{
    return [... _items];
  }

  void addFood(){
  
    notifyListeners();
  }
}