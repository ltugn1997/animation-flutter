import 'package:app_food/models/food_item.dart';
import 'package:flutter/material.dart';
import 'package:app_food/units/constant.dart';

class Foodtype{
  final String image;
  final String title;
  final Color color;
  final Color colorText;
  final String profileImage;
  final List<Food> foods;
  final List type;
  

  Foodtype({
    @required this.image,
    @required this.title,
    @required this.color,
    @required this.colorText,
    @required this.profileImage,
    @required this.foods,
    @required this.type,
  });
}

class Foodtypes with ChangeNotifier{
  List<Foodtype> _items = [
    Foodtype( image: 'assets/tuna.png', title:'Japanese',
      color:Colors.red.withOpacity(0.5),
      colorText:Colors.red,
      profileImage:'assets/plate1.png',
      foods:Contants.dishItems,
      type:['Pasta', 'Pizza', 'Basilico', 'Panzenella', 'Bruschetta'],  
    ),
     Foodtype( image: 'assets/pizza.png', title:'Italian',
      color: Colors.orange.withOpacity(0.5),
      colorText:Colors.orange,
      profileImage:'assets/plate2.png',
      foods:Contants.dishItems,
      type:['Pasta', 'Pizza', 'Basilico', 'Panzenella', 'Bruschetta'],  
    ),
     Foodtype( image: 'assets/shrimp.png', title:'Chinese',
      color:Colors.blue.withOpacity(0.5),
      colorText:Colors.blue,
      profileImage: 'assets/plate3.png',
      foods:Contants.dishItems,
      type:['Pasta', 'Pizza', 'Basilico', 'Panzenella', 'Bruschetta'],  
    ),
     Foodtype( image: 'assets/vegetables.png', title:'America',
      color:Colors.green.withOpacity(0.5),
      colorText:Colors.green,
      profileImage:'assets/plate4.png',
      foods:Contants.dishItems,
      type:['Pasta', 'Pizza', 'Basilico', 'Panzenella', 'Bruschetta'],  
    ),
  ];

  List<Foodtype> get items{
    return [... _items];
  }
}