import 'package:app_food/widgets/button_caculator.dart';
import 'package:flutter/material.dart';
class MyOrderCard extends StatefulWidget {
  final String imgURl ;
  final String cost;
  final String name;
  final double entrance;
  final int quantity;
  MyOrderCard({Key key, this.imgURl, this.cost, this.name, this.entrance, this.quantity}) : super(key: key);


  @override
  _MyOrderCardState createState() => _MyOrderCardState();
}

class _MyOrderCardState extends State<MyOrderCard> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(top: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          FittedBox(
            child: 
              Row(children: [
                Stack(children: [
                  Container(
                    height: 180,
                    width: 180,
                    child: Image.asset(widget.imgURl, fit: BoxFit.contain,),
                  ),
                  Positioned(
                    top: size.height * 0.2,
                    left: 15,
                    child: ButtonCaculator(quantity: widget.quantity,),),                 
                ],),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('\$' + widget.cost, style: TextStyle(color: Colors.greenAccent, fontSize: 20, fontWeight: FontWeight.bold),),
                    SizedBox(height: 5,),
                    DefaultTextStyle(style: Theme.of(context).textTheme.headline6, child: Text(widget.name, style: TextStyle(fontWeight: FontWeight.bold),),),
                    DefaultTextStyle(style: Theme.of(context).textTheme.bodyText1, child:Text('Entrance' + widget.entrance.toString() + 'g', style: TextStyle(color: Colors.grey[600]),),)
                  ],
                ),
                
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.all(10),
            child: Icon(Icons.close, color: Colors.grey,),
          )
      ],)
    );
  }
}