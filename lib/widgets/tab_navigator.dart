import 'package:app_food/screens/detail_item.dart';
import 'package:app_food/screens/italian_food.dart';
import 'package:app_food/screens/my_order.dart';
import 'package:app_food/screens/welcome.dart';
import 'package:app_food/widgets/tab_item.dart';
import 'package:flutter/material.dart';


class TabNavigatorRoutes {
  static const String root = '/';
  static const String detail_item = '/detail_item';
  static const String italian_item = '/italian_item';
  static const String my_order = '/my_order';
}

String checkroute(TabItem tabItem){
  if(tabItem == TabItem.home)
  {
    return TabNavigatorRoutes.root;
  }
  if(tabItem == TabItem.order)
  {
    return TabNavigatorRoutes.my_order;
  }
  if(tabItem == TabItem.user)
  {
    return TabNavigatorRoutes.root;
  }
  return '/';
}

class TabNavigator extends StatelessWidget {
  TabNavigator({this.navigatorKey, this.tabItem});
  final GlobalKey<NavigatorState> navigatorKey;
  final TabItem tabItem;

  void _push(BuildContext context, agrument, route) {
    var routeBuilders = _routeBuilders(context, agrument: agrument);

    Navigator.push(context, MaterialPageRoute(
        builder: (context) => routeBuilders[route](context),
      ),
    );
  }

  Map<String, WidgetBuilder> _routeBuilders(BuildContext context,
      {agrument}) {
    return {
      TabNavigatorRoutes.root: (context) => WelcomeScreen(
        onPush: (materialIndex, route) =>
                _push(context, materialIndex, route),
          ),
      TabNavigatorRoutes.detail_item: (context) => DetailItem(argument: agrument),
      TabNavigatorRoutes.my_order: (context) => MyOrder(),
      TabNavigatorRoutes.italian_item: (context) => ItalianFood(
           onPush: (materialIndex, route) =>
                _push(context, materialIndex, route),
            argument: agrument
      ),
    };
  }

  @override
  Widget build(BuildContext context) {
    final routeBuilders = _routeBuilders(context); 
    return Navigator(
      key: navigatorKey,
      initialRoute: TabNavigatorRoutes.root,
      onGenerateRoute: (routeSettings) {
        return MaterialPageRoute(
          builder: (context) => routeBuilders[checkroute(tabItem)](context),
        );
      },
    );
  }
}
