import 'package:app_food/Providers/food_type.dart';
import 'package:app_food/units/constant.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Categories extends StatelessWidget {
  Function(dynamic, String) onPush;
  Categories({this.onPush});
  @override
  Widget build(BuildContext context) {
    final Foodtypes foodtypes = Provider.of<Foodtypes>(context, listen: false);
    final dishs = foodtypes.items; 
    Size size = MediaQuery.of(context).size;
    return Container(
        margin: EdgeInsets.only(top: 20),
        height: size.height * 0.15,
        width: MediaQuery.of(context).size.width,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemBuilder: (ctx, i) {
            Foodtype dataFoodItem = dishs[i];
            return InkWell(
              onTap: (){onPush(dataFoodItem, '/italian_item');},
              child: Container(
                width: size.width * 0.2,
                margin: EdgeInsets.only(
                  right: 20,
                ),
                decoration: BoxDecoration(
                  color: dataFoodItem.color,
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(top: 10, right: 10, left: 10),
                      child: Hero(
                      tag: dataFoodItem.profileImage +'type',
                        child: Image.asset(
                          dataFoodItem.image,
                        ),
                      ),
                    ),
                    Text(
                      dataFoodItem.title, 
                      style: TextStyle(fontWeight: FontWeight.bold,
                      color: dataFoodItem.colorText),
                      ),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                )
              ),
            );
          },
          itemCount: Contants.carts.length,
        ),
    );
  }
}
