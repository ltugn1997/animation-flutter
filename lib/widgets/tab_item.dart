enum TabItem { home, order, user }

const Map<TabItem, String> tabName = {
  TabItem.home: 'home',
  TabItem.order: 'order',
  TabItem.user: 'user',
};