import 'package:app_food/Providers/cart.dart';
import 'package:app_food/widgets/badge.dart';
import 'package:app_food/widgets/tab_item.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BottomNavigation extends StatelessWidget {
  BottomNavigation({@required this.onSelectTab, this.currentTab});
  final TabItem currentTab;
  final ValueChanged<TabItem> onSelectTab;

  @override
  Widget build(BuildContext context) {
    Color _colorTabMatching(TabItem item) {
      return currentTab == item ? Colors.greenAccent : Colors.grey;
    }
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      items: [
        BottomNavigationBarItem(
          icon: Icon(
            Icons.restaurant,
            color: _colorTabMatching(TabItem.home)
          ),
          title:Container(
            height: 0.0,
          )
        ),
        BottomNavigationBarItem(
          icon: Consumer<Cart>(builder: (_, cart, child) => 
            Badge(
              child: child,
              color: _colorTabMatching(TabItem.order),
              value: cart.itemCount.toString(),
            ),
            child: Icon(Icons.shopping_basket),
          ), 
          title:Container(
            height: 0.0,
          )
        ),
        BottomNavigationBarItem(
          icon: Icon(
            Icons.person,
            color: _colorTabMatching(TabItem.user),
          ),
          title:Container(
            height: 0.0,
          )
        )
      ],
     
      onTap: (index) => onSelectTab(
        TabItem.values[index],
      ),
    );
  }
}


  
