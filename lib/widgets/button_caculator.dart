import 'package:flutter/material.dart';

class ButtonCaculator extends StatefulWidget {
  IconData icon;
  Color colorIcon;
  Color backGroundIcon;
  int quantity;
  ButtonCaculator({Key key, this.icon, this.colorIcon, this.backGroundIcon, this.quantity}) : super(key: key);

  @override
  _ButtonCaculatorState createState() => _ButtonCaculatorState();
}

class _ButtonCaculatorState extends State<ButtonCaculator> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        InkWell(
          onTap: () {},
          child: Container(
            height: 35.0,
            width: 35.0,
            decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.only(topLeft: Radius.circular(7), bottomLeft: Radius.circular(7)),
            color: Colors.white,
          ),
            child: Center(
            child: Icon(Icons.remove),
            ),     
          ),
        ),
        Container(
          height: 35,
          width: 35,
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
            color: Colors.white,
            ),
          child: Center(
            child: Text(widget.quantity.toString(), style: TextStyle(fontSize: 15.0)),
          ),
        ),
        InkWell(
          onTap: () {},
          child: Container(
            height: 35.0,
            width: 35.0,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.only(topRight: Radius.circular(7), bottomRight: Radius.circular(7)),
              color: Colors.white,
            ),
            child: Center(
              child: Icon(Icons.add),
            ),     
          ),
        )
      ],
    );
  }
}
