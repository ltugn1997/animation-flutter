import 'package:flutter/material.dart';
class RecommendCard extends StatelessWidget {
  final String imgURl ;
  final String cost;
  final String name;
  final double entrance;
  const RecommendCard({Key key, this.imgURl, this.cost, this.name, this.entrance}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            FittedBox(
        child: 
           Row(children: [
            Container(
              height: 80,
              width: 80,
              child: Image.asset(imgURl, fit: BoxFit.contain,),
            ),
            SizedBox(width: 20,),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('\$$cost', style: TextStyle(color: Colors.greenAccent, fontSize: 20, fontWeight: FontWeight.bold),),
                SizedBox(height: 5,),
                DefaultTextStyle(style: Theme.of(context).textTheme.headline6, child: Text(name, style: TextStyle(fontWeight: FontWeight.bold),),),
                DefaultTextStyle(style: Theme.of(context).textTheme.bodyText1, child:Text('Entrance $entrance' + 'g', style: TextStyle(color: Colors.grey[600]),),)
              ],
            ),
           ],),
            ),
            Container(
             decoration: BoxDecoration(
               borderRadius: BorderRadius.circular(15),
               color: Colors.greenAccent
             ),
             child: Icon(Icons.add, color: Colors.white,),
           )
        ],)
      
    );
  }
}