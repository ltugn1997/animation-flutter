import 'package:flutter/material.dart';

class Food {
  final String id;
  final String image;
  final String name;
  final double cost;
  final double calorie;
  final double squirrels;
  final double fats;
  final double entrance;
  final String description;
  final List<String> rawMaterial;

  Food({
    @required this.id,
    @required this.image,
    @required this.name,
    @required this.cost,
    @required this.calorie,
    @required this.squirrels,
    @required this.fats,
    @required this.entrance,
    @required this.description,
    @required this.rawMaterial,
  });
}